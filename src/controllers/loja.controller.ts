import { Request, Response } from "express";
import {MYSQL} from "../db/mysql";
import { LojaDAO } from "../DAO/lojaDao";
import { MysqlError } from "mysql";
import { ILoja } from "../interfaces/ILoja";

export class Loja {

    public Listar(req: Request, res: Response): void {
        const dao = new LojaDAO(new MYSQL().Connect());
        dao.Get((err: MysqlError, result: any[]) => {
            if (err) {
                console.log(`Erro ao consultar no banco: ${err}`);
                res.status(500).send(err);
                return;
            }
            res.status(200).json(result);
        });
    }

    public ListarPorId(req: Request, res: Response): void {
        const dao = new LojaDAO(new MYSQL().Connect());
        let { id } = req.params;
        
        dao.GetById(id, (err: MysqlError, result: any[]) => {
            if (err) {
                console.log(`Erro ao consultar no banco: ${err}`);
                res.status(500).send(err);
                return;
            }
            res.status(200).json(result);
        });
    }

    public Inserir(req: Request, res: Response): void {
        const dao = new LojaDAO(new MYSQL().Connect());
        let loja: ILoja = req.body;
        
        dao.Insert(loja, (err: MysqlError, result: any[]) => {
            if (err) {
                console.log(`Èrro ao consultar no banco: ${err}`);
                res.status(500).send(err);
                return;
            }
            res.status(200).json({
                message: "Loja inserido com sucesso!"
            });
        });
    }

    public Excluir(req: Request, res: Response): void {
        let { id } = req.params;
        const dao = new LojaDAO(new MYSQL().Connect());
        
        dao.Delete(id, (err: MysqlError, result: any[]) => {
            if (err) {
                console.log(`Èrro ao consultar no banco: ${err}`);
                res.status(500).send(err);
                return;
            }
            res.status(200).json({
                message: "Loja excluído com sucesso!"
            });
        });
    }
}