import { Request, Response } from "express";
import {MYSQL} from "../db/mysql";
import { ClienteDAO } from "../DAO/clienteDao";
import { MysqlError } from "mysql";
import { ICliente } from "../interfaces/ICliente";

export class Cliente {

    public Listar(req: Request, res: Response): void {
        const dao = new ClienteDAO(new MYSQL().Connect());
        dao.Get((err: MysqlError, result: any[]) => {
            if (err) {
                console.log(`Erro ao consultar no banco: ${err}`);
                res.status(500).send(err);
                return;
            }
            res.status(200).json(result);
        });
    }

    public ListarPorId(req: Request, res: Response): void {
        const dao = new ClienteDAO(new MYSQL().Connect());
        let { id } = req.params;
        
        dao.GetById(id, (err: MysqlError, result: any[]) => {
            if (err) {
                console.log(`Erro ao consultar no banco: ${err}`);
                res.status(500).send(err);
                return;
            }
            res.status(200).json(result);
        });
    }

    public Inserir(req: Request, res: Response): void {
        const dao = new ClienteDAO(new MYSQL().Connect());
        let cliente: ICliente = req.body;
        
        dao.Insert(cliente, (err: MysqlError, result: any[]) => {
            if (err) {
                console.log(`Èrro ao consultar no banco: ${err}`);
                res.status(500).send(err);
                return;
            }
            res.status(200).json({
                message: "Cliente inserido com sucesso!"
            });
        });
    }

    public Excluir(req: Request, res: Response): void {
        let { id } = req.params;
        const dao = new ClienteDAO(new MYSQL().Connect());
        
        dao.Delete(id, (err: MysqlError, result: any[]) => {
            if (err) {
                console.log(`Èrro ao consultar no banco: ${err}`);
                res.status(500).send(err);
                return;
            }
            res.status(200).json({
                message: "Cliente excluído com sucesso!"
            });
        });
    }
}