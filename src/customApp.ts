import * as express from "express";
import { Rotas } from "./routes/Rotas";
import * as bodyParser from "body-parser";

export class CustomApp {

    private app: express.Application;
    private route: Rotas;

    constructor() {
        this.app = express();
        this.middleware();
        this.route = new Rotas();
        this.route.Routes(this.app);
    }
    
    middleware(): void {
        this.app.use(bodyParser.urlencoded({extended: true}));
        this.app.use(bodyParser.json());
    }

    public getApp(): express.Application {
        return this.app;
    }

}