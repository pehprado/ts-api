export interface ICliente {
    Id: number;
    Nome: string;
    CPF: string;
    Endereco: string;
}