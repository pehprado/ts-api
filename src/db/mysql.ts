import * as mysql from "mysql";

export class MYSQL {

    public Connect(): mysql.Connection {
        return mysql.createConnection({
            host: "localhost",
            port: 3306,
            database: "api",
            user: "root",
            password: "root"
        });
    }

}