import { Connection, Query, MysqlError } from "mysql";
import { ILoja } from "../interfaces/ILoja";

export class LojaDAO {
    private connection: Connection;

    constructor (connection: Connection) {
        this.connection = connection;
    }

    public Get(callback: (err: MysqlError, ...args: any[]) => void): Query {
        return this.connection.query("SELECT * FROM loja", callback);
    }

    public GetById(id: number, callback: (err: MysqlError, ...args: any[]) => void) : Query {
        return this.connection.query("SELECT * FROM loja WHERE Id = ?", [id], callback);
    }

    public Insert(loja: ILoja, callback: (err: MysqlError, ...args: any[]) => void): Query {
        return this.connection.query("INSERT INTO loja SET ?", loja, callback);
    }

    public Update(id: number, loja: ILoja, callback: (err: MysqlError, ...args: any[]) => void): Query {
        return this.connection.query("UPDATE loja SET Nome = ? WHERE Id = ?", [loja.Nome, id], callback);
    }

    public Delete(id: number, callback: (err: MysqlError, ...args: any[]) => void): Query {
        return this.connection.query("DELETE FROM loja WHERE Id = ?", id, callback);
    }
 }