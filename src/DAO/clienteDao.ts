import { Connection, Query, MysqlError } from "mysql";
import { ICliente } from "../interfaces/ICliente";

export class ClienteDAO {
    private connection: Connection;

    constructor (connection: Connection) {
        this.connection = connection;
    }

    public Get(callback: (err: MysqlError, ...args: any[]) => void): Query {
        return this.connection.query("SELECT * FROM cliente", callback);
    }

    public GetById(id: number, callback: (err: MysqlError, ...args: any[]) => void) : Query {
        return this.connection.query("SELECT * FROM cliente WHERE Id = ?", [id], callback);
    }

    public Insert(cliente: ICliente, callback: (err: MysqlError, ...args: any[]) => void): Query {
        return this.connection.query("INSERT INTO cliente SET ?", cliente, callback);
    }

    public Update(id: number, cliente: ICliente, callback: (err: MysqlError, ...args: any[]) => void): Query {
        return this.connection.query("UPDATE cliente SET Nome = ?, CPF = ?, Endereco = ? WHERE Id = ?", [cliente.Nome, cliente.CPF, cliente.Endereco, id], callback);
    }

    public Delete(id: number, callback: (err: MysqlError, ...args: any[]) => void): Query {
        return this.connection.query("DELETE FROM cliente WHERE Id = ?", id, callback);
    }
 }