import { Cliente } from "../controllers/cliente.controller";
import { Loja } from "../controllers/loja.controller";

export class Rotas {

    private clienteCtrl: Cliente = new Cliente();
    private lojaCtrl: Loja = new Loja();

    public Routes(app): void {

        app.route("/api/v1/cliente")
            .get(this.clienteCtrl.Listar)
            .post(this.clienteCtrl.Inserir);

        app.route("/api/v1/cliente/:id")
           .get(this.clienteCtrl.ListarPorId)
           .delete(this.clienteCtrl.Excluir);

        app.route("/api/v1/loja")
           .get(this.lojaCtrl.Listar)
           .post(this.lojaCtrl.Inserir);

       app.route("/api/v1/loja/:id")
          .get(this.lojaCtrl.ListarPorId)
          .delete(this.lojaCtrl.Excluir);
    }

}