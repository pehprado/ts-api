import { CustomApp } from "./customApp";
import { Application, Request, Response } from "express";

const app: Application = new CustomApp().getApp();
const port = process.env.PORT || 3000;

app.get("/", (req: Request, res: Response) => {
    res.status(200)
       .send({
           message: "API OK"
       });
});

app.listen(port, _ => console.log(`Servidor rodando na porta ${port}`));