"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mysql = require("mysql");
var MYSQL = /** @class */ (function () {
    function MYSQL() {
    }
    MYSQL.prototype.Connect = function () {
        return mysql.createConnection({
            host: "localhost",
            port: 3306,
            database: "api",
            user: "root",
            password: "root"
        });
    };
    return MYSQL;
}());
exports.MYSQL = MYSQL;
