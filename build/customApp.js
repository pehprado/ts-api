"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var Rotas_1 = require("./routes/Rotas");
var bodyParser = require("body-parser");
var CustomApp = /** @class */ (function () {
    function CustomApp() {
        this.app = express();
        this.middleware();
        this.route = new Rotas_1.Rotas();
        this.route.Routes(this.app);
    }
    CustomApp.prototype.middleware = function () {
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(bodyParser.json());
    };
    CustomApp.prototype.getApp = function () {
        return this.app;
    };
    return CustomApp;
}());
exports.CustomApp = CustomApp;
