"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mysql_1 = require("../db/mysql");
var clienteDao_1 = require("../DAO/clienteDao");
var Cliente = /** @class */ (function () {
    function Cliente() {
    }
    Cliente.prototype.Listar = function (req, res) {
        var dao = new clienteDao_1.ClienteDAO(new mysql_1.MYSQL().Connect());
        dao.Get(function (err, result) {
            if (err) {
                console.log("Erro ao consultar no banco: " + err);
                res.status(500).send(err);
                return;
            }
            res.status(200).json(result);
        });
    };
    Cliente.prototype.ListarPorId = function (req, res) {
        var dao = new clienteDao_1.ClienteDAO(new mysql_1.MYSQL().Connect());
        var id = req.params.id;
        dao.GetById(id, function (err, result) {
            if (err) {
                console.log("Erro ao consultar no banco: " + err);
                res.status(500).send(err);
                return;
            }
            res.status(200).json(result);
        });
    };
    Cliente.prototype.Inserir = function (req, res) {
        var dao = new clienteDao_1.ClienteDAO(new mysql_1.MYSQL().Connect());
        var cliente = req.body;
        dao.Insert(cliente, function (err, result) {
            if (err) {
                console.log("\u00C8rro ao consultar no banco: " + err);
                res.status(500).send(err);
                return;
            }
            res.status(200).json({
                message: "Cliente inserido com sucesso!"
            });
        });
    };
    Cliente.prototype.Excluir = function (req, res) {
        var id = req.params.id;
        var dao = new clienteDao_1.ClienteDAO(new mysql_1.MYSQL().Connect());
        dao.Delete(id, function (err, result) {
            if (err) {
                console.log("\u00C8rro ao consultar no banco: " + err);
                res.status(500).send(err);
                return;
            }
            res.status(200).json({
                message: "Cliente excluído com sucesso!"
            });
        });
    };
    return Cliente;
}());
exports.Cliente = Cliente;
