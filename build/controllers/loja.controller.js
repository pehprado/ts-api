"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mysql_1 = require("../db/mysql");
var lojaDao_1 = require("../DAO/lojaDao");
var Loja = /** @class */ (function () {
    function Loja() {
    }
    Loja.prototype.Listar = function (req, res) {
        var dao = new lojaDao_1.LojaDAO(new mysql_1.MYSQL().Connect());
        dao.Get(function (err, result) {
            if (err) {
                console.log("Erro ao consultar no banco: " + err);
                res.status(500).send(err);
                return;
            }
            res.status(200).json(result);
        });
    };
    Loja.prototype.ListarPorId = function (req, res) {
        var dao = new lojaDao_1.LojaDAO(new mysql_1.MYSQL().Connect());
        var id = req.params.id;
        dao.GetById(id, function (err, result) {
            if (err) {
                console.log("Erro ao consultar no banco: " + err);
                res.status(500).send(err);
                return;
            }
            res.status(200).json(result);
        });
    };
    Loja.prototype.Inserir = function (req, res) {
        var dao = new lojaDao_1.LojaDAO(new mysql_1.MYSQL().Connect());
        var loja = req.body;
        dao.Insert(loja, function (err, result) {
            if (err) {
                console.log("\u00C8rro ao consultar no banco: " + err);
                res.status(500).send(err);
                return;
            }
            res.status(200).json({
                message: "Loja inserido com sucesso!"
            });
        });
    };
    Loja.prototype.Excluir = function (req, res) {
        var id = req.params.id;
        var dao = new lojaDao_1.LojaDAO(new mysql_1.MYSQL().Connect());
        dao.Delete(id, function (err, result) {
            if (err) {
                console.log("\u00C8rro ao consultar no banco: " + err);
                res.status(500).send(err);
                return;
            }
            res.status(200).json({
                message: "Loja excluído com sucesso!"
            });
        });
    };
    return Loja;
}());
exports.Loja = Loja;
