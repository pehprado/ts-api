"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var customApp_1 = require("./customApp");
var app = new customApp_1.CustomApp().getApp();
var port = process.env.PORT || 3000;
app.get("/", function (req, res) {
    res.status(200)
        .send({
        message: "API OK"
    });
});
app.listen(port, function (_) { return console.log("Servidor rodando na porta " + port); });
