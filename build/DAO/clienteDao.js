"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ClienteDAO = /** @class */ (function () {
    function ClienteDAO(connection) {
        this.connection = connection;
    }
    ClienteDAO.prototype.Get = function (callback) {
        return this.connection.query("SELECT * FROM cliente", callback);
    };
    ClienteDAO.prototype.GetById = function (id, callback) {
        return this.connection.query("SELECT * FROM cliente WHERE Id = ?", [id], callback);
    };
    ClienteDAO.prototype.Insert = function (cliente, callback) {
        return this.connection.query("INSERT INTO cliente SET ?", cliente, callback);
    };
    ClienteDAO.prototype.Update = function (id, cliente, callback) {
        return this.connection.query("UPDATE cliente SET Nome = ?, CPF = ?, Endereco = ? WHERE Id = ?", [cliente.Nome, cliente.CPF, cliente.Endereco, id], callback);
    };
    ClienteDAO.prototype.Delete = function (id, callback) {
        return this.connection.query("DELETE FROM cliente WHERE Id = ?", id, callback);
    };
    return ClienteDAO;
}());
exports.ClienteDAO = ClienteDAO;
