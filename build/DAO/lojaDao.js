"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LojaDAO = /** @class */ (function () {
    function LojaDAO(connection) {
        this.connection = connection;
    }
    LojaDAO.prototype.Get = function (callback) {
        return this.connection.query("SELECT * FROM loja", callback);
    };
    LojaDAO.prototype.GetById = function (id, callback) {
        return this.connection.query("SELECT * FROM loja WHERE Id = ?", [id], callback);
    };
    LojaDAO.prototype.Insert = function (loja, callback) {
        return this.connection.query("INSERT INTO loja SET ?", loja, callback);
    };
    LojaDAO.prototype.Update = function (id, loja, callback) {
        return this.connection.query("UPDATE loja SET Nome = ? WHERE Id = ?", [loja.Nome, id], callback);
    };
    LojaDAO.prototype.Delete = function (id, callback) {
        return this.connection.query("DELETE FROM loja WHERE Id = ?", id, callback);
    };
    return LojaDAO;
}());
exports.LojaDAO = LojaDAO;
