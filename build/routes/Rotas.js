"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var cliente_controller_1 = require("../controllers/cliente.controller");
var loja_controller_1 = require("../controllers/loja.controller");
var Rotas = /** @class */ (function () {
    function Rotas() {
        this.clienteCtrl = new cliente_controller_1.Cliente();
        this.lojaCtrl = new loja_controller_1.Loja();
    }
    Rotas.prototype.Routes = function (app) {
        app.route("/api/v1/cliente")
            .get(this.clienteCtrl.Listar)
            .post(this.clienteCtrl.Inserir);
        app.route("/api/v1/cliente/:id")
            .get(this.clienteCtrl.ListarPorId)
            .delete(this.clienteCtrl.Excluir);
        app.route("/api/v1/loja")
            .get(this.lojaCtrl.Listar)
            .post(this.lojaCtrl.Inserir);
        app.route("/api/v1/loja/:id")
            .get(this.lojaCtrl.ListarPorId)
            .delete(this.lojaCtrl.Excluir);
    };
    return Rotas;
}());
exports.Rotas = Rotas;
